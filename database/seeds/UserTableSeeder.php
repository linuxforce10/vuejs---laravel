<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Fabian',
            'email' => 'Fabian Castillo',
            'password' =>'1234'
        ]);
    }
}
